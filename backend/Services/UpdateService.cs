﻿using backend.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace backend.Services
{
    /**
     * UpdateService class that contains methods to communicate CRUD operations with the database.
     **/
    public class UpdateService
    {
        private readonly IMongoCollection<User> _users;

        public UpdateService(IGitFitDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.UsersCollectionName);
        }

        /**
         * Adds a Workouthistory object for that user and returns the User object
         **/
        public User AddWorkout(string id, WorkoutHistory history)
        {
            var model = _users.Find(user => user.AuthIdentifier == id).FirstOrDefault();
            model.WorkoutHistories.Add(history);
            model.RecommendedWorkouts = RecommendWorkout(model, history);
            _users.ReplaceOne(user => user.AuthIdentifier == model.AuthIdentifier, model);
            return model;
        }

        /**
         * Adds a recommended workout for that user and returns a list of WorkoutExercise objects for that user.
         **/
        private List<WorkoutExercise> RecommendWorkout(User user, WorkoutHistory latest)
        {
            var recc = new List<WorkoutExercise>();
            foreach (var exercise in latest.WorkoutExercises)
            {
                var ex = new WorkoutExercise();
                ex.Id = exercise.Id;
                ex.ExerciseName = exercise.ExerciseName;
                ex.Reps = exercise.Reps;
                if (latest.Feedback == "Too Easy")
                {
                    ex.Reps += 3;
                }
                else if (latest.Feedback == "Too Hard")
                {
                    ex.Reps -= 3;
                }

                if (ex.Reps < 5) ex.Reps = 5;
                else if (ex.Reps > 30) ex.Reps = 30;
                recc.Add(ex);
            }
            return recc;
        }

        /**
         * Adds a BMI object for that user and returns the User object.
         **/
        public User AddBMI(string id, BMI bmi)
        {
            var model = _users.Find(user => user.AuthIdentifier == id).FirstOrDefault();
            model.BMIs.Add(bmi);
            _users.ReplaceOne(user => user.AuthIdentifier == model.AuthIdentifier, model);
            return model;
        }

        /**
         * Adds a body fat value for that user and returns the User object.
         **/
        public User AddBodyFats(string id, int fats)
        {
            var model = _users.Find(user => user.AuthIdentifier == id).FirstOrDefault();
            model.Bodyfats.Add(fats);
            _users.ReplaceOne(user => user.AuthIdentifier == model.AuthIdentifier, model);
            return model;
        }
    }
}
