﻿using System;
using backend.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace backend.Services
{
    /**
     * UserService class that contains methods to communicate CRUD operations with the database.
     **/
    public class UserService
    {
        private readonly IMongoCollection<User> _users;
        private ExerciseService _exercises;

        public UserService(IGitFitDatabaseSettings settings, ExerciseService exercises)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _exercises = exercises;
        }

        /**
         * Returns a list of User objects from the database.
         **/
        public List<User> Get() =>
            _users.Find(user => true).ToList();

        /**
         * Returns an User object filtered by id from the database.
         **/
        public User Get(string id) =>
            _users.Find<User>(user => user.AuthIdentifier == id).FirstOrDefault();

        /**
         * Creates an User object in the database.
         **/
        public User Create(User user)
        {
            user.RecommendedWorkouts = InitWorkouts(user);
            _users.InsertOne(user);
            return user;
        }

        /**
         * Initializes workouts for a user and returns a list of WorkoutExercise
         **/
        private List<WorkoutExercise> InitWorkouts(User user)
        {
            var reps = user.Gender == 'M' ? 2 : 0;
            var years = DateTime.Today.Year - user.Dob.Year;
            if (years < 55)
            {
                reps += 20;
            }
            else if (years < 65)
            {
                reps += 15;
            }
            else if (years < 75)
            {
                reps += 13;
            }
            else if (years < 85)
            {
                reps += 11;
            }
            else if (years < 95)
            {
                reps += 10;
            }
            else
            {
                reps += 9;
            }

            if (user.BMIs != null && user.BMIs.Count > 0)
            {
                var htwt = user.BMIs.FindLast(x => true);
                var bmi = htwt.Weight / (htwt.Height * htwt.Height);
                if (bmi < 23)
                {
                    reps += 3;
                }
                else if (bmi >= 27.5)
                {
                    reps += 1;
                }
            }

            if (reps < 5) reps = 5;
            else if (reps > 30) reps = 30;
            var exercises = _exercises.Get();
            return exercises.Select(x =>
            {
                var ex = new WorkoutExercise();
                ex.Id = x.Id;
                ex.ExerciseName = x.ExerciseName;
                ex.Reps = reps;
                return ex;
            }).ToList();
        }

        /**
         * Updates an User object filtered by id in the database.
         **/
        public void Update(string id, User userIn)
        {
            userIn.RecommendedWorkouts = InitWorkouts(userIn);
            _users.ReplaceOne(user => user.AuthIdentifier == id, userIn); 
        }

        /**
         * Removes an User object filtered by the User object in the database.
         **/
        public void Remove(User userIn) =>
            _users.DeleteOne(user => user.AuthIdentifier == userIn.AuthIdentifier);

        /**
         * Removes an User object filtered by id in the database.
         **/
        public void Remove(string id) =>
            _users.DeleteOne(user => user.AuthIdentifier == id);
    }
}
