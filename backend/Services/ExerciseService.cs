﻿using backend.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Services
{
    /**
     * ExerciseService class that contains methods to communicate CRUD operations with the database.
     **/
    public class ExerciseService
    {
        private readonly IMongoCollection<Exercise> _exercises;

        public ExerciseService(IGitFitDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _exercises = database.GetCollection<Exercise>(settings.ExercisesCollectionName);
        }

        /**
         * Returns a list of Exercise objects from the database.
         **/
        public List<Exercise> Get() =>
            _exercises.Find(exercise => true).ToList();

        /**
         * Returns an Exercise object filtered by id from the database.
         **/
        public Exercise Get(string id) =>
            _exercises.Find<Exercise>(exercise => exercise.Id == id).FirstOrDefault();

        /**
         * Creates an Exercise object in the database.
         **/
        public Exercise Create(Exercise exercise)
        {
            _exercises.InsertOne(exercise);
            return exercise;
        }

        /**
         * Updates an Exercise object filtered by id in the database.
         **/
        public void Update(string id, Exercise exerciseIn) =>
            _exercises.ReplaceOne(exercise => exercise.Id == id, exerciseIn);

        /**
         * Removes an Exercise object filtered by the Exercise object in the database.
         **/
        public void Remove(Exercise exerciseIn) =>
            _exercises.DeleteOne(exercise => exercise.Id == exerciseIn.Id);

        /**
         * Removes an Exercise object filtered by id in the database.
         **/
        public void Remove(string id) =>
            _exercises.DeleteOne(exercise => exercise.Id == id);
    }
}
