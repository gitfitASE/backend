using System.Collections.Generic;
using System.Linq;
using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    /**
     * Update API Controller
     **/
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateController : ControllerBase
    {
        private readonly UpdateService _updateService;

        public UpdateController(UpdateService updateService)
        {
            _updateService = updateService;
        }

        /**
         * HTTP POST METHOD
         * Creates a WorkoutHistory object in the database
         **/
        [Authorize]
        [HttpPost("workout")]
        public ActionResult<User> AddWorkoutHistory(WorkoutHistory workoutHistory)
        {
            var tokenid = User.Claims
                .First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value
                .Split('|')[1];

            var model = _updateService.AddWorkout(tokenid, workoutHistory);
            return Ok(model);
        }

        /**
         * HTTP POST METHOD
         * Adds a BMI object for that user in the database
         **/
        [Authorize]
        [HttpPost("bmi")]
        public ActionResult<User> AddBMI(BMI bmi)
        {
            var tokenid = User.Claims
                .First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value
                .Split('|')[1];

            var model = _updateService.AddBMI(tokenid, bmi);
            return Ok(model);
        }

        /**
         * HTTP POST METHOD
         * Adds a Bodyfats object for that user in the database
         **/
        [Authorize]
        [HttpPost("bodyfat")]
        public ActionResult<User> AddBodyfats(int fats)
        {
            var tokenid = User.Claims
                .First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value
                .Split('|')[1];

            var model = _updateService.AddBodyFats(tokenid, fats);
            return Ok(model);
        }

    }
}