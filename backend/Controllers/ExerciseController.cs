﻿using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    /**
     * Exercise API Controller
     **/
    [Route("api/[controller]")]
    [ApiController]
    public class ExercisesController : ControllerBase
    {
        private readonly ExerciseService _exerciseService;

        public ExercisesController(ExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
        }

        /**
         * HTTP GET METHOD
         * Returns a list of all Exercise objects in database
         **/
        [Authorize]
        [HttpGet]
        public ActionResult<List<Exercise>> Get() =>
            _exerciseService.Get();

        /**
         * HTTP GET METHOD
         * Returns an Exercise object filtered by the id
         **/
        [Authorize]
        [HttpGet("{id:length(24)}", Name = "GetExercise")]
        public ActionResult<Exercise> Get(string id)
        {
            var exercise = _exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return exercise;
        }

        /**
         * HTTP POST METHOD
         * Creates an Exercise object in the database
         **/
        [Authorize]
        [HttpPost]
        public ActionResult<Exercise> Create(Exercise exercise)
        {
            _exerciseService.Create(exercise);

            return CreatedAtRoute("GetExercise", new { id = exercise.Id.ToString() }, exercise);
        }

        /**
         * HTTP PUT METHOD
         * Updates an Exercise object in the database, filtered by id
         **/
        [Authorize]
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Exercise exerciseIn)
        {
            var exercise = _exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            _exerciseService.Update(id, exerciseIn);

            return NoContent();
        }

        /**
         * HTTP DELETE METHOD
         * Deletes an Exercise object in the database specified by the id
         **/
        [Authorize]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var exercise = _exerciseService.Get(id);

            if (exercise == null)
            {
                return NotFound();
            }

            _exerciseService.Remove(exercise.Id);

            return NoContent();
        }
    }
}