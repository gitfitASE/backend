﻿using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using MongoDB.Bson;

namespace backend.Controllers
{
    /**
     * Exercise API Controller
     **/
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        /**
         * HTTP GET METHOD
         * Returns a list of all User objects in database
         **/
        [Authorize]
        [HttpGet]
        public ActionResult<List<User>> Get() => _userService.Get();

        /**
         * HTTP GET METHOD
         * Returns an User object filtered by the id
         **/
        [Authorize]
        [HttpGet("{id:length(21)}", Name = "GetUser")]
        public ActionResult<User> Get(string id)
        {
            var tokenid = User.Claims.First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value.Split('|')[1];
            if (id != tokenid)
            {
                return Unauthorized("Token ID mismatch!");
            }     
            var user = _userService.Get(id);
            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        /**
         * HTTP GET METHOD
         * Returns a list of RecommendedWorkouts aka WorkoutExercises object for that user, filtered by the id
         **/
        [Authorize]
        [HttpGet("recommended/{id:length(21)}", Name = "GetWorkout")]
        public ActionResult<List<WorkoutExercise>> GetWorkout(string id)
        {
            var tokenid = User.Claims.First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value.Split('|')[1];
            if (id != tokenid)
            {
                return Unauthorized("Token ID mismatch!");
            }     
            var user = _userService.Get(id);
            if (user == null)
            {
                return NotFound();
            }

            return user.RecommendedWorkouts;
        }

        /**
         * HTTP POST METHOD
         * Creates an UserReqModel object in the database
         **/
        [HttpPost]
        public ActionResult<User> Create(UserReqModel userReq)
        {
            var user = userReq.user;
            if (userReq.key != "kind-of-confidential-super-secret")
            {
                return Unauthorized("Token ID mismatch!");
            }

            _userService.Create(user);

            return CreatedAtRoute("GetUser", new { id = user.AuthIdentifier }, user);
        }

        /**
        * HTTP PUT METHOD
        * Updates an User object in the database, filtered by id
        **/
        [Authorize]
        [HttpPut("{id:length(21)}")]
        public IActionResult Update(string id, User userIn)
        {
            var tokenid = User.Claims.First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value.Split('|')[1];
            if (id != tokenid || id != userIn.AuthIdentifier)
            {
                return Unauthorized("Token ID mismatch!");
            } 
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            userIn.Id = user.Id;
            userIn.RecommendedWorkouts = user.RecommendedWorkouts;
            userIn.WorkoutHistories = user.WorkoutHistories;

            _userService.Update(id, userIn);

            return Ok();
        }

        /**
        * HTTP DELETE METHOD
        * Delete an User object in the database, filtered by id
        **/
        [Authorize]
        [HttpDelete("{id:length(21)}")]
        public IActionResult Delete(string id)
        {
            var tokenid = User.Claims.First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value.Split('|')[1];
            if (id != tokenid)
            {
                return Unauthorized("Token ID mismatch!");
            } 
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            _userService.Remove(user.Id);

            return NoContent();
        }
    }
}