﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace backend.Models
{
    public class WorkoutExercise : Exercise
    {
        public int Reps { get; set; }
    }
}
