﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace backend.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        [JsonProperty("Name")]
        public string Name { get; set; }

        public string AuthIdentifier { get; set; }

        public char Gender { get; set; }

        public DateTime Dob { get; set; }

        public List<BMI> BMIs { get; set; }
        public List<int> Bodyfats { get; set; }
        public char Smoker { get; set; }
        public string Ailments { get; set; }
        public string Injuries { get; set; }
        public List<WorkoutHistory> WorkoutHistories {get; set;}
        public List<WorkoutExercise> RecommendedWorkouts { get; set; }

        public User()
        {
            
        }
    }
}
