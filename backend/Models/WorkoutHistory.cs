﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace backend.Models
{
    public class WorkoutHistory
    {
        public float Duration { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Feedback { get; set; }
        public List<WorkoutExercise> WorkoutExercises { get; set; }
    }
}