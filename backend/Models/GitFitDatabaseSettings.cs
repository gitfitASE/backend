﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    /**
     *  GitFitDatabaseSettings contains the tables for each corresponding collection in the database.
     **/
    public class GitFitDatabaseSettings : IGitFitDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string ExercisesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IGitFitDatabaseSettings
    {
        string UsersCollectionName { get; set; }
        string ExercisesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
