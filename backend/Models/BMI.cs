﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace backend.Models
{
    public class BMI
    {
        public DateTime Date { get; set; }
        public float Height { get; set; }
        public float Weight { get; set; }
    }
}
