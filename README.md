# GitFit Backend

GitFit is a fitness application using Unity and Microsoft Kinect to help elderly remain fit with minimal guidance and intervention from caretakers. It gamifies the exercise process and provides clear guidance and feedback on exercise performance through the use of the Kinect sensor. GitFit also includes an online analytics server that analyses workout performance to recommend customised workouts for the individual. This analytics data can be viewed anywhere through a Web Dashboard which displays the user data upon login.

This repository houses the backend server for GitFit, written in .NET Core. Its main responsibilities include:
* Analysing the incoming workout data
* Interfacing with a NoSQL database to store and serve analytics data and workout history
* Authenticating users via Auth0 for user data privacy.

## Getting started

### Prerequisites

* .NET Core v3.1 minimum  
Download .NET Core for your operating system [here](https://dotnet.microsoft.com/download) 

```shell
dotnet --version
```

To check if your downloaded version is above 3.1

* Docker  
The application is meant to be run as a docker container, and having Docker would be convenient for development. Follow the instructions 
to download Docker [here](https://www.docker.com/products/docker-desktop).

### Installation

* Clone the repository to your local machine using Git  

Using HTTPS:
```shell
git clone https://gitlab.com/gitfitASE/backend.git
```
Using SSH:
```shell
git clone git@gitlab.com:gitfitASE/backend.git
```
 
* Run `dotnet restore` in the root directory to restore NuGet dependencies


### How to run
1. Navigate to the root directory of the project.
2. Build the images if the image is outdated using `docker build . -f dockerfile-release`
3. Start the docker containers for the application using `docker run <IMAGE_NAME>`

## How to Contribute?

To contribute, please assign yourself to any open issue on the Kanban board and create a new branch from `development`. **Do not** make changes directly in the `development` branch. The repository is configured to reject all pushes to development. Write unit tests where applicable. All features and fixes should be merged into development via a Merge Request. Wait for the Backend Principal or Tech Lead to review the code and approve the merge request. Merges into production are prohibited.

Being a project written in C#, please primarily follow C# code conventions. Good general guidelines can be found
[here](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions).


## Building

After any code changes, you can build the project by running `dotnet build` in the root folder

Alternatively, if you have Docker installed, you can run `docker build . -t <IMAGE_NAME> -f dockerfile-release` to build a docker image tagged
with your image name.

## Testing
Navigate to the root directory and run `dotnet test`.

## Running Locally
You can run the project locally by running `dotnet run` to test its behaviour locally.

## Deploying / Publishing

Deployment is managed entirely using Gitlab's CI pipeline. The deployment pipeline will be run fortnightly as per the development schedule.

## Links

Will be updated with any relevant links.

## Troubleshooting

Will be updated with any issues.
